'use strict';
/**
 * A react representation of the old index.jade page. I tried to re-use some
 * of the code from global.js
 */

var React = require('react');
var ReactDOM = require('react-dom');
var $ = require('jquery');

function index() {
  var didFetch = false;//Useful to know if it is the first time we're fetching data.

  //States declaration
  const [userListData, setUserListData] = React.useState([]);
  const [username, setUsername] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [fullName, setFullName] = React.useState('');
  const [age, setAge] = React.useState('');
  const [location, setLocation] = React.useState('');
  const [gender, setGender] = React.useState('');
  const [selectedUserIndex, setSelectedUserIndex] = React.useState(null);

  function populateTable() {
    // jQuery AJAX call for JSON
    $.getJSON('/users/userlist', function (data) {
      if (didFetch) {
        setSelectedUserIndex(data.length - 1);
      } else {
        setSelectedUserIndex(null);
      }
      didFetch = true;
      // Stick our user data array into a userlist variable in the global object
      setUserListData(data);
    });
  }

  React.useEffect(
    function () {
      populateTable();
    },
    [didFetch]
  );

  // Add User
  function addUser(event) {
    event.preventDefault();

    // Super basic validation - increase errorCount variable if any fields are blank
    var errorCount = 0;
    if (username.length == 0) errorCount++;
    if (email.length == 0) errorCount++;
    if (fullName.length == 0) errorCount++;
    if (age.length == 0) errorCount++;
    if (location.length == 0) errorCount++;
    if (gender.length == 0) errorCount++;

    // Check and make sure errorCount's still at zero
    if (errorCount === 0) {
      // If it is, compile all user info into one object
      var newUser = {
        username: username,
        email: email,
        fullname: fullName,
        age: age,
        location: location,
        gender: gender,
      };

      // Use AJAX to post the object to our adduser service
      $.ajax({
        type: 'POST',
        data: newUser,
        url: '/users/adduser',
        dataType: 'JSON',
      }).done(function (response) {
        // Check for successful (blank) response
        if (response.msg === '') {
          // Clear the form inputs
          $('#addUser fieldset input').val('');
          // Update the table
          populateTable();
        } else {
          // If something goes wrong, alert the error message that our service returned
          alert('Error: ' + response.msg);
        }
      });
    } else {
      // If errorCount is more than 0, error out
      alert('Please fill in all fields');
      return false;
    }
  }

  // Delete User
  function deleteUser(event, id) {
    event.preventDefault();

    // Pop up a confirmation dialog
    var confirmation = confirm('Are you sure you want to delete this user?');

    // Check and make sure the user confirmed
    if (confirmation === true) {
      // If they did, do our delete
      $.ajax({
        type: 'DELETE',
        url: '/users/deleteuser/' + id,
      }).done(function (response) {
        // Check for a successful (blank) response
        if (response.msg === '') {
        } else {
          alert('Error: ' + response.msg);
        }

        // Update the table
        populateTable();
      });
    } else {
      // If they said no to the confirm, do nothing
      return false;
    }
  }

  function showUserInfo(index) {
    setSelectedUserIndex(index);
  }

  return (
    <div>
      <h1>Express</h1>
      <p>Welcome to our test</p>
      {/* <!-- Wrapper--> */}
      <div id="wrapper">
        {/* <!-- USER INFO--> */}
        <div id="userInfo">
          <h2>User Info</h2>
          <p>
            <strong>Name:</strong>{' '}
            {userListData[selectedUserIndex] != undefined
              ? userListData[selectedUserIndex].fullname
              : ''}{' '}
            <span id="userInfoName"></span>
            <br />
            <strong>Age:</strong>{' '}
            {userListData[selectedUserIndex] != undefined
              ? userListData[selectedUserIndex].age
              : ''}{' '}
            <span id="userInfoAge"></span>
            <br />
            <strong>Gender:</strong>{' '}
            {userListData[selectedUserIndex] != undefined
              ? userListData[selectedUserIndex].gender
              : ''}{' '}
            <span id="userInfoGender"></span>
            <br />
            <strong>Location:</strong>{' '}
            {userListData[selectedUserIndex] != undefined
              ? userListData[selectedUserIndex].location
              : ''}{' '}
            <span id="userInfoLocation"></span>
          </p>
        </div>
        {/* <!-- /USER INFO-->
    <!-- USER LIST--> */}
        <h2>User List</h2>
        <div id="userList">
          <table>
            <thead>
              <th>UserName</th>
              <th>Email</th>
              <th>Delete?</th>
            </thead>
            <tbody>
              {userListData.map(function (user, index) {
                return (
                  <tr>
                    <td>
                      <a
                        href="#"
                        class="linkshowuser"
                        rel={user['username']}
                        title="Show Details"
                        onClick={function (e) {
                          showUserInfo(index);
                        }}
                      >
                        {user.username}
                      </a>
                    </td>
                    <td>{user.email}</td>
                    <td>
                      <a
                        href="#"
                        class="linkdeleteuser"
                        onClick={function (e) {
                          deleteUser(e, user._id);
                        }}
                      >
                        delete
                      </a>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        {/* <!-- /USER LIST-->
    <!-- ADD USER--> */}
        <h2>Add User</h2>
        <div id="addUser">
          <fieldset>
            <input
              onChange={(e) => setUsername(e.target.value)}
              id="inputUserName"
              type="text"
              placeholder="Username"
            />
            <input
              onChange={(e) => setEmail(e.target.value)}
              id="inputUserEmail"
              type="text"
              placeholder="Email"
            />
            <br />
            <input
              onChange={(e) => setFullName(e.target.value)}
              id="inputUserFullname"
              type="text"
              placeholder="Full Name"
            />
            <input
              onChange={(e) => setAge(e.target.value)}
              id="inputUserAge"
              type="text"
              placeholder="Age"
            />
            <br />
            <input
              onChange={(e) => setLocation(e.target.value)}
              id="inputUserLocation"
              type="text"
              placeholder="Location"
            />
            <input
              onChange={(e) => setGender(e.target.value)}
              id="inputUserGender"
              type="text"
              placeholder="gender"
            />
            <br />
            <button onClick={addUser} id="btnAddUser">
              Add User
            </button>
          </fieldset>
        </div>
        {/* <!-- /ADD USER--> */}
      </div>
      {/* <!-- /WRAPPER--> */}
    </div>
  );
}

const domContainer = document.querySelector('#root');
ReactDOM.render(React.createElement(index), domContainer);
