/**
 * Middleware to check if the body exist and if there's data in it.
 */
function bodyMiddleware(req, res, next) {
  if (req.body && Object.keys(req.body).length > 0) {
    next();
  } else {
    res
      .status(400)
      .send({ msg: 'This route requires the body to not be null.' });
  }
}

exports.bodyMiddleware = bodyMiddleware;
