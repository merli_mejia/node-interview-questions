var expect = require('expect');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
var monk = require('monk');

var url = 'http://localhost:3000';

describe('Update User', function () {
  var updatedUser = {
    username: 'test user',
    email: 'test3@test.com', //new email!
    fullname: 'Bob Smith',
    age: 27,
    location: 'San Francisco',
    gender: 'Male',
  };

  var db = monk('localhost:27017/node_interview_question');
  var collection = db.get('userlist');
  var userId = null;
  it("updates the 'test user' from the database", function (done) {
    collection.findOne({ username: 'test user' }, function (err, doc) {
      userId = doc._id;
      request(url)
        .put('/users/updateuser/' + userId)
        .send(updatedUser)
        .expect(200)
        .end(function (err, res) {
          if (err) {
            console.log(JSON.stringify(res));
            throw err;
          }
          db.close();
          done();
        });
    });
  });
  it('Should return 400 by not passing a body', function (done) {
    request(url)
      .put('/users/updateuser/' + userId)
      .send()
      .expect(400)
      .end(function (err, res) {
        if (err) {
          console.log(JSON.stringify(res));
          throw err;
        }
        done();
      });
  });
});
