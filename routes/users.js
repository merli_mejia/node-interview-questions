var express = require('express');
var router = express.Router();
var middlewares = require('../middlewares/users');

/*
 * GET userlist.
 */
router.get('/userlist', function (req, res) {
  var db = req.db;
  var collection = db.get('userlist');
  collection.find({}, {}, function (err, docs) {
    if (!err) {
      res.json(docs);
    } else {
      res.status(500).send({ msg: err });
    }
  });
});

/*
 * POST to adduser.
 */
router.post('/adduser', middlewares.bodyMiddleware, function (req, res) {
  var db = req.db;
  var collection = db.get('userlist');
  collection.insert(req.body, function (err, result) {
    if (!err) {
      res.status(201).send({ msg: '' });
    } else {
      res.status(500).send({ msg: err });
    }
  });
});

/*
 * DELETE to deleteuser.
 */
router.delete('/deleteuser/:id', function (req, res) {
  var db = req.db;
  var collection = db.get('userlist');
  var userToDelete = req.params.id;
  collection.remove({ _id: userToDelete }, function (err) {
    if (!err) {
      res.status(200).send({ msg: '' });
    } else {
      res.status(500).send({ msg: err });
    }
  });
});

/*
 * PUT to updateuser.
 */
router.put('/updateuser/:id', middlewares.bodyMiddleware, function (req, res) {
  var db = req.db;
  var collection = db.get('userlist');
  var userToUpdate = req.params.id;

  collection.findOneAndUpdate({ _id: userToUpdate }, req.body, function (err) {
    if (!err) {
      res.status(200).send({ msg: '' });
    } else {
      res.status(500).send({ msg: err });
    }
  });
});

module.exports = router;
