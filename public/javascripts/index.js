'use strict';
/**
 * A react representation of the old index.jade page. I tried to re-use some
 * of the code from global.js
 */

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var React = require('react');
var ReactDOM = require('react-dom');
var $ = require('jquery');

function index() {
  var didFetch = false; //Useful to know if it is the first time we're fetching data.

  //States declaration

  var _React$useState = React.useState([]),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      userListData = _React$useState2[0],
      setUserListData = _React$useState2[1];

  var _React$useState3 = React.useState(''),
      _React$useState4 = _slicedToArray(_React$useState3, 2),
      username = _React$useState4[0],
      setUsername = _React$useState4[1];

  var _React$useState5 = React.useState(''),
      _React$useState6 = _slicedToArray(_React$useState5, 2),
      email = _React$useState6[0],
      setEmail = _React$useState6[1];

  var _React$useState7 = React.useState(''),
      _React$useState8 = _slicedToArray(_React$useState7, 2),
      fullName = _React$useState8[0],
      setFullName = _React$useState8[1];

  var _React$useState9 = React.useState(''),
      _React$useState10 = _slicedToArray(_React$useState9, 2),
      age = _React$useState10[0],
      setAge = _React$useState10[1];

  var _React$useState11 = React.useState(''),
      _React$useState12 = _slicedToArray(_React$useState11, 2),
      location = _React$useState12[0],
      setLocation = _React$useState12[1];

  var _React$useState13 = React.useState(''),
      _React$useState14 = _slicedToArray(_React$useState13, 2),
      gender = _React$useState14[0],
      setGender = _React$useState14[1];

  var _React$useState15 = React.useState(null),
      _React$useState16 = _slicedToArray(_React$useState15, 2),
      selectedUserIndex = _React$useState16[0],
      setSelectedUserIndex = _React$useState16[1];

  function populateTable() {
    // jQuery AJAX call for JSON
    $.getJSON('/users/userlist', function (data) {
      if (didFetch) {
        setSelectedUserIndex(data.length - 1);
      } else {
        setSelectedUserIndex(null);
      }
      didFetch = true;
      // Stick our user data array into a userlist variable in the global object
      setUserListData(data);
    });
  }

  React.useEffect(function () {
    populateTable();
  }, [didFetch]);

  // Add User
  function addUser(event) {
    event.preventDefault();

    // Super basic validation - increase errorCount variable if any fields are blank
    var errorCount = 0;
    if (username.length == 0) errorCount++;
    if (email.length == 0) errorCount++;
    if (fullName.length == 0) errorCount++;
    if (age.length == 0) errorCount++;
    if (location.length == 0) errorCount++;
    if (gender.length == 0) errorCount++;

    // Check and make sure errorCount's still at zero
    if (errorCount === 0) {
      // If it is, compile all user info into one object
      var newUser = {
        username: username,
        email: email,
        fullname: fullName,
        age: age,
        location: location,
        gender: gender
      };

      // Use AJAX to post the object to our adduser service
      $.ajax({
        type: 'POST',
        data: newUser,
        url: '/users/adduser',
        dataType: 'JSON'
      }).done(function (response) {
        // Check for successful (blank) response
        if (response.msg === '') {
          // Clear the form inputs
          $('#addUser fieldset input').val('');
          // Update the table
          populateTable();
        } else {
          // If something goes wrong, alert the error message that our service returned
          alert('Error: ' + response.msg);
        }
      });
    } else {
      // If errorCount is more than 0, error out
      alert('Please fill in all fields');
      return false;
    }
  }

  // Delete User
  function deleteUser(event, id) {
    event.preventDefault();

    // Pop up a confirmation dialog
    var confirmation = confirm('Are you sure you want to delete this user?');

    // Check and make sure the user confirmed
    if (confirmation === true) {
      // If they did, do our delete
      $.ajax({
        type: 'DELETE',
        url: '/users/deleteuser/' + id
      }).done(function (response) {
        // Check for a successful (blank) response
        if (response.msg === '') {} else {
          alert('Error: ' + response.msg);
        }

        // Update the table
        populateTable();
      });
    } else {
      // If they said no to the confirm, do nothing
      return false;
    }
  }

  function showUserInfo(index) {
    setSelectedUserIndex(index);
  }

  return React.createElement(
    'div',
    null,
    React.createElement(
      'h1',
      null,
      'Express'
    ),
    React.createElement(
      'p',
      null,
      'Welcome to our test'
    ),
    React.createElement(
      'div',
      { id: 'wrapper' },
      React.createElement(
        'div',
        { id: 'userInfo' },
        React.createElement(
          'h2',
          null,
          'User Info'
        ),
        React.createElement(
          'p',
          null,
          React.createElement(
            'strong',
            null,
            'Name:'
          ),
          ' ',
          userListData[selectedUserIndex] != undefined ? userListData[selectedUserIndex].fullname : '',
          ' ',
          React.createElement('span', { id: 'userInfoName' }),
          React.createElement('br', null),
          React.createElement(
            'strong',
            null,
            'Age:'
          ),
          ' ',
          userListData[selectedUserIndex] != undefined ? userListData[selectedUserIndex].age : '',
          ' ',
          React.createElement('span', { id: 'userInfoAge' }),
          React.createElement('br', null),
          React.createElement(
            'strong',
            null,
            'Gender:'
          ),
          ' ',
          userListData[selectedUserIndex] != undefined ? userListData[selectedUserIndex].gender : '',
          ' ',
          React.createElement('span', { id: 'userInfoGender' }),
          React.createElement('br', null),
          React.createElement(
            'strong',
            null,
            'Location:'
          ),
          ' ',
          userListData[selectedUserIndex] != undefined ? userListData[selectedUserIndex].location : '',
          ' ',
          React.createElement('span', { id: 'userInfoLocation' })
        )
      ),
      React.createElement(
        'h2',
        null,
        'User List'
      ),
      React.createElement(
        'div',
        { id: 'userList' },
        React.createElement(
          'table',
          null,
          React.createElement(
            'thead',
            null,
            React.createElement(
              'th',
              null,
              'UserName'
            ),
            React.createElement(
              'th',
              null,
              'Email'
            ),
            React.createElement(
              'th',
              null,
              'Delete?'
            )
          ),
          React.createElement(
            'tbody',
            null,
            userListData.map(function (user, index) {
              return React.createElement(
                'tr',
                null,
                React.createElement(
                  'td',
                  null,
                  React.createElement(
                    'a',
                    {
                      href: '#',
                      'class': 'linkshowuser',
                      rel: user['username'],
                      title: 'Show Details',
                      onClick: function onClick(e) {
                        showUserInfo(index);
                      }
                    },
                    user.username
                  )
                ),
                React.createElement(
                  'td',
                  null,
                  user.email
                ),
                React.createElement(
                  'td',
                  null,
                  React.createElement(
                    'a',
                    {
                      href: '#',
                      'class': 'linkdeleteuser',
                      onClick: function onClick(e) {
                        deleteUser(e, user._id);
                      }
                    },
                    'delete'
                  )
                )
              );
            })
          )
        )
      ),
      React.createElement(
        'h2',
        null,
        'Add User'
      ),
      React.createElement(
        'div',
        { id: 'addUser' },
        React.createElement(
          'fieldset',
          null,
          React.createElement('input', {
            onChange: function onChange(e) {
              return setUsername(e.target.value);
            },
            id: 'inputUserName',
            type: 'text',
            placeholder: 'Username'
          }),
          React.createElement('input', {
            onChange: function onChange(e) {
              return setEmail(e.target.value);
            },
            id: 'inputUserEmail',
            type: 'text',
            placeholder: 'Email'
          }),
          React.createElement('br', null),
          React.createElement('input', {
            onChange: function onChange(e) {
              return setFullName(e.target.value);
            },
            id: 'inputUserFullname',
            type: 'text',
            placeholder: 'Full Name'
          }),
          React.createElement('input', {
            onChange: function onChange(e) {
              return setAge(e.target.value);
            },
            id: 'inputUserAge',
            type: 'text',
            placeholder: 'Age'
          }),
          React.createElement('br', null),
          React.createElement('input', {
            onChange: function onChange(e) {
              return setLocation(e.target.value);
            },
            id: 'inputUserLocation',
            type: 'text',
            placeholder: 'Location'
          }),
          React.createElement('input', {
            onChange: function onChange(e) {
              return setGender(e.target.value);
            },
            id: 'inputUserGender',
            type: 'text',
            placeholder: 'gender'
          }),
          React.createElement('br', null),
          React.createElement(
            'button',
            { onClick: addUser, id: 'btnAddUser' },
            'Add User'
          )
        )
      )
    )
  );
}

var domContainer = document.querySelector('#root');
ReactDOM.render(React.createElement(index), domContainer);